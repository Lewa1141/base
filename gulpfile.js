var syntax        = 'sass'; // Syntax: sass or scss;

var gulp = require('gulp'),
    plugins = {};
Object.keys(require('./package.json')['devDependencies'])
    .filter(function (pkg) { return pkg.indexOf('browser-') === 0 || pkg.indexOf('gulp-') === 0 ; })
    .forEach(function (pkg) {
        plugins[pkg.replace('gulp-', '').replace(/-/g, '_')] = require(pkg);
    });
	paths = {
    pages: ['src/*.html']};

gulp.task('browser-sync', function() {
	plugins.browser_sync({
		server: {
			baseDir: 'app'
		},
		notify: false,
		//open: false,
		//online: false, // Work Offline Without Internet Connection
		tunnel: true, 
		tunnel: "defeaad", // Demonstration page: http://"tunnel".localtunnel.me
	})
});

gulp.task('styles', function() {
	return gulp.src('app/'+syntax+'/**/*.'+syntax+'')
	.pipe(plugins.sass({ outputStyle: 'expanded' }).on("error", plugins.notify.onError()))
	.pipe(plugins.rename({ suffix: '.min', prefix : '' }))
	.pipe(plugins.autoprefixer(['last 15 versions']))
	.pipe(plugins.clean_css( {level: { 1: { specialComments: 0 } } })) // Opt., comment out when debugging
	.pipe(gulp.dest('app/css'))
	.pipe(plugins.browser_sync.stream())
});

gulp.task('js', function() {
	return gulp.src([
		'app/libs/jquery/dist/jquery.min.js',
		'app/js/common.js', // Always at the end
		])
	.pipe(plugins.concat('scripts.min.js'))
	.pipe(plugins.uglify()) // Mifify js (opt.)
	.pipe(gulp.dest('app/js'))
	.pipe(plugins.browser_sync.reload({ stream: true }))
});

gulp.task('rsync', function() {
	return gulp.src('app/**')
	.pipe(plugins.rsync({
		root: 'app/',
		hostname: 'username@yousite.com',
		destination: 'yousite/public_html/',
		include: ['*.htaccess'], // Includes files to deploy
		exclude: ['**/Thumbs.db', '**/*.DS_Store'], // Excludes files from deploy
		recursive: true,
		archive: true,
		silent: false,
		compress: true
	}))
});

gulp.task('copyHtml', function () {
    return gulp.src(paths.pages)
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', ['styles', 'js', 'browser-sync'], function() {
	gulp.watch('app/'+syntax+'/**/*.'+syntax+'', ['styles']);
	gulp.watch(['libs/**/*.js', 'app/js/common.js'], ['js']);
	gulp.watch('app/*.html', plugins.browser_sync.reload)
});

gulp.task('default', ['watch']);
